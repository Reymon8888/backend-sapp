const express = require('express');
const router = express.Router();
const TabUsuario = require('../models/TabUsuario');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const key = require('../security/Config');
const { json } = require('body-parser');

const BCRYPT_SALT_ROUNDS = 10; // Rondas de sal

// Crear usuario nuevo

router.post('/usuarios/nuevo', (req, res) => {

    // metodo para encriptar password

    bcrypt.hash(req.body.contrasena, BCRYPT_SALT_ROUNDS, (err, contrasenaEncriptada) => {
        if(err) {
            console.log('-----------------------------------------------------------------------------')
            console.log('ALGO SALIO MAL - DATO NO ENCRIPTADO');
        } else {
            TabUsuario.create({ // Se crea nuevo registro
                usuario: req.body.usuario,
                contrasena: contrasenaEncriptada,
                correo: req.body.correo,
                rolId: req.body.rol,
                estatus: req.body.estatus
            },
            { 
                logging: false
            }).then( post => {  // Se genera el token para el nuevo usuario
                const token = jwt.sign({
                    id: post.id,
                    usuario: post.usuario,
                    correo: post.correo,
                    rol: post.rolId,
                    estatus: post.estatus,
                    mensaje: 'Token de acceso válido'

                }, key.secret, { // Se define el tiempo de expiración de token
                    expiresIn: key.segundos * key.minutos
                });
                res.json({
                    id: post.id,
                    usuario: post.usuario,
                    correo: post.correo,
                    estatus: post.estatus,
                    rol: post.rol,
                    auth: true,
                    token: token
                });
                console.log('-----------------------------------------------------------------------------')
                console.log('SE HA INSERTADO UN NUEVO USUARIO CORRECTAMENTE');
            })
        }
    });
});

// Listar todos los registros

router.get('/usuarios/todos', (req, res) => {
    TabUsuario.findAll({
        include:
            {
            association: TabUsuario.CatRol,
            as: 'rol'
        }, logging: false 
    }).then( post => {
        if(post.length < 1){
            res.json({mensaje: 'Aún no hay usuarios registrados en la base de datos'})
            console.log('-----------------------------------------------------------------------------')
            console.log('AUN NO HAY USUARIOS EN LA BASE DE DATOS');
        }else{
            res.json(post);
            console.log('-----------------------------------------------------------------------------')
            console.log('SE HAN OBTENIDO TODOS LOS USUARIOS CORRECTAMENTE');
        }
        
    })
});


// Listar registros activos

router.get('/usuarios/lista', (req, res) => {
    TabUsuario.findAll({
        where: {
            estatus: 1
        },
        include: [ 
            {
            association: TabUsuario.CatRol
        }
        ], logging: false
    }).then( post => {
        if(post.length < 1){
            res.json({mensaje: 'Aún no hay usuarios activos en la base de datos'})
            console.log('-----------------------------------------------------------------------------')
            console.log('AUN NO HAY USUARIOS ACTIVOS EN LA BASE DE DATOS');
        }else{
            res.json(post);
            console.log('-----------------------------------------------------------------------------')
            console.log('SE HAN OBTENIDO LOS USUARIOS ACTIVOS CORRECTAMENTE');
        }
    })
});

// Buscar por id

router.get('/usuarios/:id', (req, res) => {
    TabUsuario.findByPk(req.params.id, { logging: false}).then( post => {
        res.json(post);
        console.log('-----------------------------------------------------------------------------')
        console.log(`EL USUARIO ENCONTRADO ES: '${post.usuario}' CON ID: '${post.id}'`);
    }).catch(error => {
        console.log('USUARIO NO ENCONTRADO');
    });
});

// Actualizar registro

router.put('/usuarios/actualizar/:id', (req, res) => {
    TabUsuario.update({
        usuario: req.body.usuario,
        contrasena: req.body.contrasena,
        correo: req.body.correo,
        rol: req.body.rol
        },  
        { 
            logging: false,
            where: {
                id: req.params.id
            }
        }).then( result => {
            res.json(result);
            console.log('-----------------------------------------------------------------------------')
            console.log(`EL USUARIO: '${req.body.usuario}' HA SIDO ACTUALIZADO CORRECTAMENTE`);
        }).catch(error => {
            console.log('ALGO HA SALIDO MAL, EL REGISTRO NO FUE ACTUALIZADO');
        });
});

// Eliminar registro

router.put('/usuarios/eliminar/:id', (req, res) => {
    TabUsuario.update({
        estatus: 0
        },  
        { 
            logging: false,
            where: {
                id: req.params.id
            }
        }).then( result => {
            res.json(result);
            console.log('-----------------------------------------------------------------------------')
            console.log('EL USUARIO HA SIDO DESHABILITADO CORRECTAMENTE');
        }).catch(error => {
            console.log('ALGO HA SALIDO MAL, EL REGISTRO NO FUE DESHABILITADO');
        });
});

// Eliminar registro permanentemente --- SOLO USAR BAJO AUTORIZACIÓN DEL CLIENTE

router.delete('/usuarios/:id', (req, res) => {
    TabUsuario.destroy( 
        { 
            logging: false,
            where: {
                id: req.params.id
            }
        }).then( result => {
            res.json(result);
            console.log('-----------------------------------------------------------------------------')
            console.log('EL USUARIO HA SIDO ELIMINADO PERMANENTEMENTE');
        })
});




module.exports = router;


