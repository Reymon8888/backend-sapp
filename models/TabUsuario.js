const { Model, DataTypes} = require('sequelize');
const sequelize = require('../database/db');
const CatRol = require('./CatRol');


// Modelo para interactuar con tabla de usuarios

class TabUsuario extends Model {}

TabUsuario.init(
    {
        usuario: DataTypes.STRING,
        contrasena: DataTypes.STRING,
        correo: DataTypes.STRING,
        estatus: DataTypes.INTEGER
        
    },
    {
        sequelize,
        modelName: 'tab_usuario'
    }
);

// Relación => Un usuario debe tener un rol

TabUsuario.CatRol = TabUsuario.belongsTo(CatRol, {foreignKey: 'rolId'} );

module.exports = TabUsuario;