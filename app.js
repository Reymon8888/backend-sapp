const fs = require('fs');
const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();
const cors = require('cors');
const sequelize = require('./database/db');


/* configuracion servidor local */
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

/* respuesta al levantar */

app.get('/api', (req, res) => res.status(200).send({
     message: 'Backend SAPPiente',
}));

//  se importan controladores

app.use('/api', require('./controllers/TabUsuarioController'));
app.use('/api', require('./controllers/CatRolController'));
app.use('/api', require('./security/AuthController'));


/* test de funcionamiento de servidores */

const port = parseInt(process.env.PORT, 10) || 4000;
app.set('port', port);
const server = http.createServer(app);
server.listen( port, function () {
     fs.readFile('./utils/banner.txt', 'utf-8', (error, datos) => {
          console.log(datos);
          sequelize.sync({ force: false, logging: false }).then(() => {
               console.log('TABLAS ACTUALIZADAS CORRECTAMENTE');
          }).catch(error => {
          console.log('HA OCURRIDO UN ERROR EN LA BASE DE DATOS', error);
          });
     });  
});

module.exports = app;