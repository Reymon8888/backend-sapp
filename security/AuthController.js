const express = require('express');
const router = express.Router();
const CatRol = require('../models/CatRol');
const jwt = require('jsonwebtoken');
const key = require('./Config');
const TabUsuario = require('../models/TabUsuario');

// Generador de token

router.post('/login', (req, res) => {
    
    const usuario = {id: 1};
    const token = jwt.sign({usuario}, key.secret, {
        expiresIn: key.segundos * key.minutos
    });

    res.json({
        auth: true,
        token: token
    });

});

// Validacion

router.get('/auth', (req, res) => {

    const token = req.headers['x-access-token'];
    if (!token) {
        console.log('-----------------------------------------------------------------------------')
        console.log('NO SE PUDO OBTENER EL TOKEN - ACCESO DENEGADO');
        return res.status(401).json({
            auth: false,
            message: 'Acceso denegado'
        });
    }
    const decoded = jwt.verify(token, key.secret);
    console.log('-----------------------------------------------------------------------------')
    console.log('TOKEN DE USUARIO VALIDO - ACCESO PERMITIDO');
    const validacion = TabUsuario.findByPk(decoded.id, {logging: false});
    if (!validacion){
        return res.status(404).send('Usuario no encontrado');
    }
    res.json(decoded)
});


module.exports = router;