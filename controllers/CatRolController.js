const express = require('express');
const router = express.Router();
const CatRol = require('../models/CatRol');

// Crear rol nuevo

router.post('/roles/nuevo', (req, res) => {
    CatRol.create({
        rol: req.body.rol,
        tipo: req.body.tipo,
        estatus: req.body.estatus
    }, {logging: false}).then( post => {
        res.json(post);
        console.log('-----------------------------------------------------------------------------')
        console.log('SE HA INSERTADO UN NUEVO ROL CORRECTAMENTE');
    })
});

// Listar todos los registros

router.get('/roles/todos', (req, res) => {
    CatRol.findAll({
        logging: false
    }).then( post => {
        if(post.length < 1){
            res.json({mensaje: 'Aún no hay roles en la base de datos'});
            console.log('-----------------------------------------------------------------------------')
            console.log('AUN NO HAY ROLES EN LA BASE DE DATOS');
        }else {
            res.json(post);
            console.log('-----------------------------------------------------------------------------')
            console.log('SE HAN OBTENIDO TODOS LOS ROLES CORRECTAMENTE');
        }
    })
});

// Listar todos los registros activos

router.get('/roles/lista', (req, res) => {
    CatRol.findAll({
        logging: false,
        where:{
            estatus: 1
        }
    }).then( post => {
        if(post.length < 1){
            res.json({mensaje: 'Aún no hay roles activos en la base de datos'});
            console.log('-----------------------------------------------------------------------------')
            console.log('AUN NO HAY ROLES EN LA BASE DE DATOS');
        }else {
            res.json(post);
            console.log('-----------------------------------------------------------------------------')
            console.log('SE HAN OBTENIDO LOS ROLES ACTIVOS CORRECTAMENTE');
        }
    })
});

// Buscar por id

router.get('/roles/:id', (req, res) => {
    CatRol.findByPk(req.params.id, {
        logging: false
    }).then( post => {
        res.json(post);
        console.log('-----------------------------------------------------------------------------')
        console.log(`EL ROL ENCONTRADO ES: '${post.rol}' CON ID: '${post.id}'`);
    })
});

// Actualizar registro

router.put('/roles/actualizar/:id', (req, res) => {
    CatRol.update({
        rol: req.body.rol,
        tipo: req.body.tipo
        },  
        { 
            logging: false,
            where: {
                id: req.params.id
            }
        }).then( result => {
            res.json(result);
            console.log('-----------------------------------------------------------------------------')
            console.log(`EL ROL: '${req.body.rol}' HA SIDO ACTUALIZADO CORRECTAMENTE`);
        })
});

// Eliminar registro

router.put('/roles/eliminar/:id', (req, res) => {
    CatRol.update({
        estatus: 0
        },  
        { 
            logging: false,
            where: {
                id: req.params.id
            }
        }).then( result => {
            res.json(result);
            console.log('-----------------------------------------------------------------------------')
            console.log('EL ROL HA SIDO DESHABILITADO CORRECTAMENTE');
        })
});

// Eliminar registro permanentemente --- SOLO USAR BAJO AUTORIZACIÓN DEL CLIENTE

router.delete('/roles/:id', (req, res) => {
    CatRol.destroy( 
        { 
            logging: false,
            where: {
                id: req.params.id
            }
        }).then( result => {
            res.json(result);
            console.log('-----------------------------------------------------------------------------')
            console.log('EL USUARIO HA SIDO DESHABILITADO CORRECTAMENTE');
        })
});

// Carga de roles definidos

router.post('/roles/cargar-roles', (req, res) => {
    CatRol.create({ id: 1, rol: 'Super Usuario', descripcion: 'SAPP', estatus: 1 }, { logging: false });
    CatRol.create({ id: 2, rol: 'Administrador', descripcion: 'SAPP', estatus: 1 }, { logging: false });
    CatRol.create({ id: 3, rol: 'Comercial', descripcion: 'SAPP', estatus: 1 }, { logging: false });
    CatRol.create({ id: 4, rol: 'Docente', descripcion: 'SAPP', estatus: 1 }, { logging: false });
    CatRol.create({ id: 5, rol: 'Tutor', descripcion: 'SAPP', estatus: 1 }, { logging: false });
    CatRol.create({ id: 6, rol: 'Alumno', descripcion: 'SAPP', estatus: 1 }, { logging: false });
    CatRol.create({ id: 7, rol: 'Publico', descripcion: 'SAPP', estatus: 1 }, { logging: false }); 
    res.json({ text: 'Catálogo cargado correctamente',
    });
    console.log('-----------------------------------------------------------------------------')
    console.log('CATALOGO DE ROLES CARGADO CORRECTAMENTE');
});


module.exports = router;

