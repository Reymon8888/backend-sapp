const { Model, DataTypes} = require('sequelize');
const sequelize = require('../database/db');

class CatRol extends Model {}

CatRol.init(
    {
        rol: DataTypes.STRING,
        descripcion: DataTypes.STRING,
        estatus: DataTypes.INTEGER
        
    },
    {
        sequelize,
        modelName: 'cat_role'
    }
);

module.exports = CatRol;